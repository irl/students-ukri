
<!-- Get the recent posts -->

<?php query_posts('posts_per_page=4'); ?>

<!-- Start the Loop. -->

<?php $homepage_post_count = 0; ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<?php $homepage_post_count += 1; ?>

<?php if ( $homepage_post_count % 2 != 0 ) { ?>
 					<div>
						<ul>
<?php } // For odd posts ?>
							<li>				
								<h3><?php the_title(); ?></h3>
								<div class="date"><?php the_time('jS F Y') ?></div>
								<p><?php echo substr(get_the_content(), 0, 85); ?>...</p><!-- TODO: This needs to be written properly, add a ... on the end and such -->
								<p><a href="<?php the_permalink() ?>" class="button">See Details</a></p>
							</li>
							
<?php if ( $homepage_post_count % 2 == 0 ) { ?>					
						</ul>
					</div>
<?php } // For even posts ?>
 
 <!-- Stop The Loop (but note the "else:" - see next line). -->

 <?php endwhile; ?> 
 
 <?php if ( $homepage_post_count % 2 != 0 ) { ?>					
						</ul>
					</div>
<?php } // In case we ended on an odd post ?>
 
 
 <?php else: ?>


 <!-- The very first "if" tested to see if there were any Posts to -->
 <!-- display.  This "else" part tells what do if there weren't any. -->
 <p>No news has been published yet.</p>


 <!-- REALLY stop The Loop. -->
 <?php endif; ?>