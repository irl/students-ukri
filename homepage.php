<?php
/**
 * @package WordPress
 * @subpackage IEEE_Web_Templates_Student
 */

/*
Template Name: Homepage
*/
?>

<?php define("IS_HOMEPAGE", TRUE); ?>

<?php get_header(); ?>

	<div id="hp-container">	

		<div id="hp-container1">
			<div class="overline"><img src="<?php bloginfo('template_url'); ?>/images/transparent.gif" width="1" height="1" alt="" /></div>
			<div id="hp_content1" class="container">
				<img src="<?php echo get_option('welcome_image'); ?>" width="227" height="177" class="image" />
				<h3>Welcome to our student branch's website</h3>
				<p>This is the official website of the <?php echo get_option('school_name'); ?>'s IEEE student branch. IEEE is the world's largest professional association advancing technology for the benefit of humanity. IEEE publishes technical journals, sponsors conferences, develops technology standards, and supports the professional interests of more than 400,000 members.</p>
				<p><a href=" <?php echo home_url(); ?>/about" class="arrow">Read more about our branch</a></p>
			</div>
		</div>

		<div id="hp-container2">
			<div class="overline"><img src="<?php bloginfo('template_url'); ?>/images/transparent.gif" width="1" height="1" alt="" /></div>
			<div id="hp_content2" class="container">
				<h2><?php echo get_option('announcement_title'); ?></h2>
				<p><?php echo get_option('announcement_content'); ?></p>
			</div>
		</div>

		<div id="hp-container3">
			<div class="overline"><img src="<?php bloginfo('template_url'); ?>/images/transparent.gif" width="1" height="1" alt="" /></div>
			<div id="hp_content3" class="container">
				<h2 class="left">Branch Events</h2>
				<div class="right">
					<img id="prev" src="<?php bloginfo('template_url'); ?>/images/icon_up.gif" width="14" height="14" alt="Up" title="Up" />
					<img id="next" src="<?php bloginfo('template_url'); ?>/images/icon_down.gif" width="14" height="14" alt="Down" title="Down" /></a>
				</div>
				<div class="clear"></div>
				<div id="slideshow">
				
					<div>
						<ul>
							<li>				
								<img src="<?php bloginfo('template_url'); ?>/images/hp/img_fpo_90.jpg" width="90" height="50" alt="FPO" title="FPO" class="image" />
								<div class="date">xx.xx.xx</div>
								<p><strong>Nam scelerisque, nisi quis ullamcorper dignissim, justo enim.</strong></p>
							</li>
							<li>				
								<img src="<?php bloginfo('template_url'); ?>/images/hp/img_fpo_90.jpg" width="90" height="50" alt="FPO" title="FPO" class="image" />
								<div class="date">xx.xx.xx</div>
								<p><strong>Nam scelerisque, nisi quis ullamcorper dignissim, justo enim.</strong></p>
							</li>
							<li>				
								<img src="<?php bloginfo('template_url'); ?>/images/hp/img_fpo_90.jpg" width="90" height="50" alt="FPO" title="FPO" class="image" />
								<div class="date">xx.xx.xx</div>
								<p><strong>Nam scelerisque, nisi quis ullamcorper dignissim, justo enim.</strong></p>
							</li>
						</ul>
					</div>
					
					<div>
						<ul>
							<li>				
								<img src="<?php bloginfo('template_url'); ?>/images/hp/img_fpo_90.jpg" width="90" height="50" alt="FPO" title="FPO" class="image" />
								<div class="date">xx.xx.xx</div>
								<p><strong>Nam scelerisque, nisi quis ullamcorper dignissim, justo enim.</strong></p>
							</li>
							<li>				
								<img src="<?php bloginfo('template_url'); ?>/images/hp/img_fpo_90.jpg" width="90" height="50" alt="FPO" title="FPO" class="image" />
								<div class="date">xx.xx.xx</div>
								<p><strong>Nam scelerisque, nisi quis ullamcorper dignissim, justo enim.</strong></p>
							</li>
							<li>				
								<img src="<?php bloginfo('template_url'); ?>/images/hp/img_fpo_90.jpg" width="90" height="50" alt="FPO" title="FPO" class="image" />
								<div class="date">xx.xx.xx</div>
								<p><strong>Nam scelerisque, nisi quis ullamcorper dignissim, justo enim.</strong></p>
							</li>
						</ul>
					</div>
					
				</div>
				<p><a href="#" class="arrow">More Branch Events</a></p>
			</div>
		</div>

		<div id="hp-container4">
			<div class="overline"><img src="<?php bloginfo('template_url'); ?>/images/transparent.gif" width="1" height="1" alt="" /></div>
			<div id="hp_content4" class="container">
				<h2 class="left">Section Events</h2>
				<div class="right">
					<img id="prev2" src="<?php bloginfo('template_url'); ?>/images/icon_up.gif" width="14" height="14" alt="Up" title="Up" />
					<img id="next2" src="<?php bloginfo('template_url'); ?>/images/icon_down.gif" width="14" height="14" alt="Down" title="Down" /></a>
				</div>
				<div class="clear"></div>
				<div id="slideshow2">
				
					<div>
						<ul>
							<li>				
								<div class="date">9th September 2013</div>
								<p><strong><a href="http://ieee-ukri.org/events?regevent_action=register&event_id=178&name_of_event=European%20Microelectronics%20&%20Packaging%20Conference%20(EMPC-2013)">European Microelectronics & Packaging Conference (EMPC-2013)</a></strong></p>
							</li>
							<li>				
								<div class="date">8th October 2013</div>
								<p><strong><a href="http://ieee-ukri.org/events?regevent_action=register&event_id=160&name_of_event=EMC%20Soc%20Demonstration%20Sessions">EMC Soc Demonstration Sessions</a></strong></p>
							</li>
							<li>				
								<div class="date">11th December 2013</div>
								<p><strong><a href="http://ieee-ukri.org/events?regevent_action=register&event_id=161&name_of_event=EMC%20Soc%20Christmas%20Meeting">EMC Soc Christmas Meeting</a></strong></p>
							</li>
							<li>				
								<div class="date">16th December 2013</div>
								<p><strong><a href="http://ieee-ukri.org/events?regevent_action=register&event_id=169&name_of_event=Fifth%20IET%20International%20Conference%20on%20Imaging%20for%20Crime%20Detection%20and%20Prevention%20(ICDP-13)">Fifth IET International Conference on Imaging for Crime Detection and Prevention (ICDP-13)</a></strong></p>
							</li>
						</ul>
					</div>
					
				</div>
				<p><a href="http://ieee-ukri.org/events" class="arrow">More Section Events</a></p>
			</div>
		</div>

		<div id="hp-container5">
			<div class="overline"><img src="<?php bloginfo('template_url'); ?>/images/transparent.gif" width="1" height="1" alt="" /></div>
			<div id="hp_content5" class="container">
				<h2>Become an IEEE Member</h2>
				<p>IEEE provides opportunities that will help you develop your professional identity.</p>
				<p><a href="https://www.ieee.org/membership_services/membership/students/index.html?WT.mc_id=rfm_stud" class="arrow">Join Now</a></p>
			</div>
		</div>

		<div id="hp-container6">
			<div class="overline"><img src="<?php bloginfo('template_url'); ?>/images/transparent.gif" width="1" height="1" alt="" /></div>
			<div id="hp_content6" class="container">
				<h2>myIEEE Login</h2>
				<form>
				<div class="small">Username</div>
				<input name="username" id="username" type="text" class="round" />
				<div class="small">Password</div>
				<input name="password" id="password" type="password" class="round" />
				
				<input name="submit" id="submit" type="submit" value="Login Now" class="button" style="margin-top: 2px;" />
				</form>
			</div>
		</div>

		<div id="hp-container7">
			<div class="overline"><img src="<?php bloginfo('template_url'); ?>/images/transparent.gif" width="1" height="1" alt="" /></div>
			<div id="hp_content7" class="container">
				<h2 class="left">Branch News</h2>
				<div class="right">
					<img id="prev3" src="<?php bloginfo('template_url'); ?>/images/icon_left.gif" width="14" height="14" alt="Up" title="Up" />
					<img id="next3" src="<?php bloginfo('template_url'); ?>/images/icon_right.gif" width="14" height="14" alt="Down" title="Down" /></a>
				</div>
				<div class="clear"></div>
				<div id="slideshow3">
				
					<?php include('homepage_loop.php'); ?>			
					
				</div>
			</div>
		</div>

		<div id="hp-container9">
			<div class="overline"><img src="<?php bloginfo('template_url'); ?>/images/transparent.gif" width="1" height="1" alt="" /></div>
			<div id="hp_content9" class="container">
				<h2>Recent Tweets</h2>
				<a class="twitter-timeline" data-dnt="true" href="https://twitter.com/<?php the_twitter_handle(); ?>"  data-widget-id="358237568476798976" data-screen-name="<?php the_twitter_handle(); ?>">Tweets by @<?php the_twitter_handle(); ?></a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

				<p><a href="http://www.twitter.com/<?php the_twitter_handle(); ?>" class="arrow">View our profile at Twitter</a></p>
			</div>
		</div>

		<div id="hp-container8">
			<div class="overline"><img src="<?php bloginfo('template_url'); ?>/images/transparent.gif" width="1" height="1" alt="" /></div>
			<div id="hp_content8" class="container">
<h2>IEEE Students on Facebook</h2>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<table border="0" style="width:100%;"><tr><td style="width:50%;text-align:left;">
<div data-header="true" data-show-border="true" data-stream="false" data-show-faces="false" data-height="200" data-width="300" data-href="https://www.facebook.com/IEEEStudents" class="fb-like-box">&nbsp;</div>
</td><td style="width:50%;text-align:left;">
<div data-header="true" data-show-border="true" data-stream="false" data-show-faces="false" data-height="200" data-width="300" data-href="https://www.facebook.com/IEEEXtreme" class="fb-like-box">&nbsp;</div>
</td></tr></table>

			</div>
		</div>

	</div>

<?php get_footer(); ?>
