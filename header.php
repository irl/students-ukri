<?php
/**
 * @package WordPress
 * @subpackage IEEE_Web_Templates_Student_UKRI
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta  http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>

<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/styles.css" type="text/css" media="screen" />
<!--[if IE]>
	<script type="text/javascript" language="javascript" src="<?php bloginfo('template_url'); ?>/scripts/jquery.min.js"></script>
	<script type="text/javascript" language="javascript" src="<?php bloginfo('template_url'); ?>/scripts/ie.js"></script>
<![endif]-->
<!--[if IE 7]>
	<link type="text/css" rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/ie7.css" media="screen,tv" />
<![endif]-->
<!--[if lte IE 6]>
	<link type="text/css" rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/ie6.css" media="screen,tv" />
<![endif]-->
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
<script type="text/javascript" language="javascript" src="<?php bloginfo('template_url'); ?>/scripts/jquery.min.js"></script>
<?php if ( IS_HOMEPAGE ) { ?>
<script type="text/javascript" language="javascript" src="<?php bloginfo('template_url'); ?>/scripts/jquery.cycle.all.min.js"></script>
<?php } // IS_HOMEPAGE ?>
<script type="text/javascript" language="javascript" src="<?php bloginfo('template_url'); ?>/scripts/global.js"></script>
<?php if ( IS_HOMEPAGE ) { ?>
	<script type="text/javascript">
		//$.noConflict();
		jQuery(document).ready(function() {			
			jQuery('#slideshow').cycle({ 
				fx: 'turnDown', // choose your transition type, ex: fade, scrollUp, shuffle, etc...
				timeout: 6000,
				prev: '#prev',
				next: '#next',
				cleartypeNoBg: true,
				pause:   1 // pauses slideshow on hover 
			});
			
			jQuery('#slideshow2').cycle({ 
				fx: 'turnDown', // choose your transition type, ex: fade, scrollUp, shuffle, etc...
				timeout: 6000,
				prev: '#prev2',
				next: '#next2',
				cleartypeNoBg: true,
				pause:   1 // pauses slideshow on hover 
			});
			
			jQuery('#slideshow3').cycle({ 
				fx: 'scrollRight', // choose your transition type, ex: fade, scrollUp, shuffle, etc...
				timeout: 6000,
				prev: '#prev3',
				next: '#next3',
				cleartypeNoBg: true,
				pause:   1 // pauses slideshow on hover 
			});
		});
	</script>
<?php } ?>
<style type="text/css">
/* Replacing "customize.css" */

a:link, a:visited {
	color:  <?php the_school_colour(); ?>;
}

a:hover, a:active {
	color:  <?php the_school_colour(); ?>;
}

#leftnav ul li a:hover {
	color:  <?php the_school_colour(); ?>;
}

#leftnav ul li a.active, #leftnav ul li.current_page_item a {
	color:  <?php the_school_colour(); ?>;
}

#header {
	background-color: <?php the_school_colour(); ?>;
}

.overline {
	background-color: <?php the_school_colour(); ?>;
}

#content th {
	background-color: <?php the_school_colour(); ?>;
	color: #fff;
}
</style>
<?php wp_head(); ?>
</head>
<body>
<!-- Global IEEE Header -->
<div id="global-hd-container">
	<div id="global-hd">
		<ul>
		<li><a href="#">IEEE.org</a></li>
		<li><a href="#">IEEE Xplore Digital Library</a></li>
		<li><a href="#">IEEE Standards Association</a></li>
		<li><a href="#">Spectrum Online</a></li>
		<li class="last"><a href="#">More IEEE Sites</a></li>
		</ul>
	</div>
</div>

<div id="wedge"></div>	
<div id="header">
	<!-- Site Logo -->
	<div id="logos">
		<div id="main">
			<h1>IEEE Student Branch</h1>
			<?php if(get_option('school_name')!=""){ ?> <h2> <?php echo get_option('school_name'); ?> </h2> <?php } ?>
			<?php if(get_option('ieee_tagline')!=""){ ?> <p> <?php echo get_option('ieee_tagline'); ?> </p> <?php } ?>
		</div>
		<div id="ieee"><a href="http://www.ieee.org/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/transparent.gif" width="125" height="37" alt="IEEE" title="IEEE" /></a></div>

		<div id="university">
			<?php
				// Check if this is a post or page, if it has a thumbnail, and if it's a big one
				if ( is_singular() &&
						has_post_thumbnail( $post->ID ) &&
						( /* $src, $width, $height */ $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnail' ) ) &&
						$image[1] >= HEADER_IMAGE_WIDTH ) :
					// Houston, we have a new header image!
					echo get_the_post_thumbnail( $post->ID, 'post-thumbnail' );
				else : ?>
					<img src="<?php header_image(); ?>" width="<?php echo HEADER_IMAGE_WIDTH; ?>" height="<?php echo HEADER_IMAGE_HEIGHT; ?>" alt="" />
			<?php endif; ?>
		</div>
		
		<!-- Sign in link -->
		<div id="links">
			<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'secondary', 'fallback_cb' => 'footer_links' ) ); ?>
		</div>
	</div>
	
	<!-- Site Navigation -->
	<div id="navbar">
		<div id="navigation">
			<!-- load widget for pages -->
			<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
		</div>
	</div>
	
	<div class="clear"></div>

	<!-- Search Bar -->
	<div id="search">
		<form method="get" id="searchform" action="<?php bloginfo('wpurl'); ?>" >
		<div id="search-content">Search <?php bloginfo( 'name' ); ?></div>
		<div class="left"><input type="text" name="s" size="25" id="searchbox" /></div><div class="srch-btn-right"></div>
		<div class="left"><div class="srch-btn-left"><a href="#" id="search-button-submit">Search</a></div><div class="btn-right"></div></div>
		</form>
		
		<!-- Social Media Icons -->		
		<div id="socialmedia">
			Follow:
			<?php if(get_option('ieee_social_facebook')!=""){ ?>
				<a href="<?php echo get_option('ieee_social_facebook'); ?>" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/icon_facebook.gif" width="23" height="23" alt="Facebook" title="Facebook" class="socialicon" /></a>
			<?php } else { ?>
				<a href="http://www.facebook.com/share.php?u=<?php echo get_permalink(); ?>" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/icon_facebook.gif" width="23" height="23" alt="Facebook" title="Facebook" class="socialicon" /></a>
			<?php } ?>
			<?php if(get_option('ieee_social_twitter')!=""){ ?>
				<a href="<?php echo get_option('ieee_social_twitter'); ?>" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/icon_twitter.gif" width="23" height="23" alt="Twitter" title="Twitter" class="socialicon" /></a>
			<?php } else { ?>
				<a href="http://twitter.com/home?status=I+found+this+very+interesting+<?php echo get_permalink(); ?>" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/icon_twitter.gif" width="23" height="23" alt="Twitter" title="Twitter" class="socialicon" /></a>
			<?php } ?>
			<?php if(get_option('ieee_social_youtube')!=""){ ?>
				<a href="<?php echo get_option('ieee_social_youtube'); ?>" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/icon_youtube.gif" width="23" height="23" alt="YouTube" title="YouTube" class="socialicon" /></a>
			<?php } else { ?>
				<a href="#" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/icon_youtube.gif" width="23" height="23" alt="YouTube" title="YouTube" class="socialicon" /></a>
			<?php } ?>
			<?php if(get_option('ieee_social_linkedin')!=""){ ?>
				<a href="<?php echo get_option('ieee_social_linkedin'); ?>" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/icon_linkedin.gif" width="23" height="23" alt="LinkedIn" title="LinkedIn" class="socialicon" /></a>
			<?php } else { ?>
				<a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo get_permalink(); ?>" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/icon_linkedin.gif" width="23" height="23" alt="LinkedIn" title="LinkedIn" class="socialicon" /></a>
			<?php } ?>
			| Share: <a href="http://addthis.com/bookmark.php?v=250&amp;username=d2creative" class="addthis_button_compact"><img src="<?php bloginfo('template_url'); ?>/images/icon_share.gif" width="23" height="23" alt="ShareThis" title="ShareThis" class="socialicon" /></a>
			<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#username=d2creative"></script> 
		</div>
	</div>
	
</div>
<div class="clear"></div>

<div class="top"></div><div id="container">
		
		<div id="sub-container2">