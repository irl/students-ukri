<?php

// Print out the school colour or a default green if not set
function the_school_colour() {
	if (get_option('school_colour'))
		echo get_option('school_colour');
	else
		echo "#00574d";
}

// Print out the Twitter handle or IEEE's twitter handle
function the_twitter_handle() {
	if (get_option('ieee_social_twitter'))
		echo basename(get_option('ieee_social_twitter'));
	else
		echo "IEEEorg";
}

// Create homepage settings menu
add_action('admin_menu', 'homepage_create_menu');

function homepage_create_menu() {

	//create new top-level menu
	add_menu_page('Homepage', 'Homepage Settings', 'administrator', __FILE__, 'homepage_settings_page',
	get_stylesheet_directory_uri('stylesheet_directory')."/images/media-button-other.gif");

	//call register settings function
	add_action( 'admin_init', 'register_homepage_settings' );
}

function register_homepage_settings() {
	//register our settings
	register_setting( 'homepage-settings-group', 'school_colour' );
	register_setting( 'homepage-settings-group', 'school_name' );
	register_setting( 'homepage-settings-group', 'welcome_image' );
	register_setting( 'homepage-settings-group', 'announcement_title' );
	register_setting( 'homepage-settings-group', 'announcement_content' );
}

function homepage_settings_page() {
?>
<div class="wrap">
<h2>Homepage Settings</h2>

<form method="post" action="options.php">
    <?php settings_fields( 'homepage-settings-group' ); ?>
    <?php //do_settings( 'homepage-settings-group' ); ?>
	<h3>General Settings</h3>
    <table class="form-table">
		<tr valign="top">
		<th scope="row">School Name</th>
		<td><input type="text" name="school_name" value="<?php echo get_option('school_name'); ?>" size="60" /> (e.g. University of Faketown)</td>
		</tr>
		
        <tr valign="top">
        <th scope="row">School Colour (hex)</th>
        <td><input type="text" name="school_colour" value="<?php the_school_colour(); ?>" /> (e.g. #00574d)</td>
        </tr>
    </table>
    
	<h3>Welcome Box</h3>
	
	<table class="form-table">
		<tr valign="top">
		<th scope="row">Welcome Image URL</th>
		<td><input type="text" name="welcome_image" value="<?php echo get_option('welcome_image'); ?>" size="80" /></td>
		</tr>
	</table>
	<p><i>The welcome image should be exactly 227x177 pixels in size. <a href="<?php echo home_url(); ?>/wp-admin/media-new.php" target="_blank">Click here</a> to upload your image.</i></p>
	
	
	<h3>Announcement Box</h3>
	<table class="form-table">
		<tr valign="top">
		<th scope="row">Title</th>
		<td><input type="text" name="announcement_title" value="<?php echo get_option('announcement_title'); ?>" size="80" /></td>
		</tr>
		
		<tr valign="top">
		<th scope="row">Content</th>
		<td><textarea name="announcement_content" rows="4" cols="80"><?php echo get_option('announcement_content'); ?></textarea></td>
		</tr>
	</table>
	
    <?php submit_button(); ?>

</form>
</div>
<?php } ?>
